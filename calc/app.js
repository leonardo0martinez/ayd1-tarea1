const express = require('express');
const app = express();
const cors = require('cors')

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended:false}));



app.post('/suma', (req, res) => {
    let sum = req.body.num1 + req.body.num2;
    res.json({"resultado": sum});
})

app.get('/date', (req, res) => {    
  let date = new Date();
  let hora = date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
  let fecha = date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear(); 
  res.json({
      hora: hora,
      fecha: fecha
  });
})

app.listen(3000, () => {
  console.log('server on: 3000');
})

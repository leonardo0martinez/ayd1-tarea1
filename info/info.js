const express = require('express');
const app = express();
const cors = require('cors')

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended:false}));



app.get('/info', (req, res) => {    
    res.json({
        nombre: "Leonardo Roney Martínez Maldonado",
        carnet: 201780044
    });
})

app.post('/resta', (req, res) => {
  let sub = req.body.num1 - req.body.num2;
  res.json({"resultado": sub});
})

app.listen(4000, () => {
  console.log('server on: 4000');
})
